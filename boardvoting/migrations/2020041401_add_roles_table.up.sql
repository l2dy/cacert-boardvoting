-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE user_roles
(
    id       INTEGER PRIMARY KEY AUTOINCREMENT,
    voter_id VARCHAR(255) NOT NULL REFERENCES voters (id),
    role     VARCHAR(8)   NOT NULL,
    created  TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (voter_id, role)
);
INSERT INTO user_roles (voter_id, role)
SELECT id, 'VOTER'
FROM voters
WHERE enabled = true;
