Dear Board,

{{ with .Decision }}The motion with the identifier {{.Tag}} has been {{.Status}}.{{ end }}

The reasoning for this result is: {{ .Reasoning }}

{{ with .Decision }}Motion:
    {{.Title}}
    {{.Content}}

Vote type: {{.VoteType}}{{end}}

{{ with .VoteSums }}	Ayes: {{ .Ayes }}
	Nayes: {{ .Nayes }}
	Abstentions: {{ .Abstains }}

	Percentage: {{ .Percent }}%{{ end }}

Kind regards,
the voting system.