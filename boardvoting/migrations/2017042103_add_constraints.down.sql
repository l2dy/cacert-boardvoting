-- SQL section 'Down' is executed when this migration is rolled back

CREATE TABLE votes_orig (
  decision INT4,
  voter    INT4,
  vote     INT4,
  voted    DATETIME,
  notes    TEXT DEFAULT ''
);
INSERT INTO votes_orig (decision, voter, vote, voted, notes)
  SELECT
    decision,
    voter,
    vote,
    voted,
    notes
  FROM votes;
DROP TABLE votes;
ALTER TABLE votes_orig
  RENAME TO votes;

CREATE TABLE decisions_orig (
  id        INTEGER PRIMARY KEY,
  proposed  DATETIME,
  proponent INTEGER,
  title     VARCHAR(255),
  content   TEXT,
  quorum    INTEGER,
  majority  INTEGER,
  status    INTEGER,
  due       DATETIME,
  modified  DATETIME,
  tag       VARCHAR(255),
  votetype  INT4 DEFAULT 0 NOT NULL
);
INSERT INTO decisions_orig (id, proposed, proponent, title, content, status, due, modified, tag, votetype)
  SELECT
    id,
    proposed,
    proponent,
    title,
    content,
    status,
    due,
    modified,
    tag,
    votetype
  FROM
    decisions;
DROP INDEX decisions_proposed_idx;
DROP TABLE decisions;
ALTER TABLE decisions_orig
  RENAME TO decisions;

CREATE TABLE emails_orig (
  voter   INT4,
  address VARCHAR(255)
);
INSERT INTO emails_orig (voter, address)
  SELECT
    voter,
    address
  FROM emails;
DROP TABLE emails;
ALTER TABLE emails_orig
  RENAME TO emails;

CREATE TABLE voters_orig (
  id       INTEGER PRIMARY KEY,
  name     VARCHAR(255),
  enabled  INTEGER DEFAULT 0,
  reminder VARCHAR(255)
);
INSERT INTO voters_orig (id, name, enabled, reminder)
  SELECT
    id,
    name,
    enabled,
    reminder
  FROM voters;
DROP TABLE voters;
ALTER TABLE voters_orig
  RENAME TO voters;