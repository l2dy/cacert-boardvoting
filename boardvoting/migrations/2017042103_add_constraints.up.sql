-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE voters_new (
  id       INTEGER PRIMARY KEY,
  name     VARCHAR(255) NOT NULL,
  enabled  BOOLEAN      NOT NULL,
  reminder VARCHAR(255)
);
INSERT INTO voters_new (id, name, enabled, reminder)
  SELECT
    id,
    name,
    enabled,
    reminder
  FROM voters;
DROP TABLE voters;
ALTER TABLE voters_new
  RENAME TO voters;

CREATE TABLE emails_new (
  voter   INTEGER             NOT NULL REFERENCES voters (id),
  address VARCHAR(255) UNIQUE NOT NULL
);
INSERT INTO emails_new (voter, address)
  SELECT
    voter,
    address
  FROM emails;
DROP TABLE emails;
ALTER TABLE emails_new
  RENAME TO emails;

CREATE TABLE decisions_new (
  id        INTEGER PRIMARY KEY,
  proposed  DATETIME            NOT NULL,
  proponent INTEGER             NOT NULL REFERENCES voters (id),
  title     VARCHAR(255)        NOT NULL,
  content   TEXT                NOT NULL,
  status    INTEGER             NOT NULL CHECK (status IN (-2, -1, 0, 1)),
  due       DATETIME            NOT NULL,
  modified  DATETIME            NOT NULL,
  tag       VARCHAR(255) UNIQUE NOT NULL,
  votetype  INTEGER DEFAULT 0 NOT NULL CHECK (votetype IN (0, 1))
);
INSERT INTO decisions_new (
  id, proposed, proponent, title, content, status, due, modified, tag, votetype
)
  SELECT
    id,
    proposed,
    proponent,
    title,
    content,
    status,
    due,
    modified,
    tag,
    votetype
  FROM decisions;
DROP TABLE decisions;
ALTER TABLE decisions_new
  RENAME TO decisions;
CREATE INDEX decisions_proposed_idx
  ON decisions (proposed);

CREATE TABLE votes_new (
  decision INTEGER REFERENCES decisions (id),
  voter    INTEGER REFERENCES voters (id),
  vote     INTEGER  NOT NULL CHECK (vote IN (-1, 0, 1)),
  voted    DATETIME NOT NULL,
  notes    TEXT     NOT NULL DEFAULT '',
  PRIMARY KEY (decision, voter)
);
INSERT INTO votes_new (decision, voter, vote, voted, notes)
  SELECT
    decision,
    voter,
    vote,
    voted,
    notes
  FROM votes;
DROP TABLE votes;
ALTER TABLE votes_new
  RENAME TO votes;
