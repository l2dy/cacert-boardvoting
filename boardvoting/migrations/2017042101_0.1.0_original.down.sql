-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE votes;
DROP TABLE decisions;
DROP TABLE emails;
DROP TABLE voters;
