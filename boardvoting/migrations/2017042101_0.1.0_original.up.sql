-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE IF NOT EXISTS votes (decision INT4, voter INT4, vote INT4, voted DATETIME, notes text default '');
CREATE TABLE IF NOT EXISTS emails (voter INT4, address VARCHAR(255));
CREATE TABLE IF NOT EXISTS voters (id INTEGER PRIMARY KEY, name VARCHAR(255), enabled INTEGER default 0, reminder  VARCHAR(255));
CREATE TABLE IF NOT EXISTS decisions (id INTEGER PRIMARY KEY, proposed DATETIME, proponent INTEGER, title VARCHAR(255), content TEXT, quorum INTEGER, majority INTEGER, status INTEGER, due DATETIME, modified DATETIME, tag varchar(255), votetype INT4 DEFAULT 0 NOT NULL);
