VERSION := $(shell git describe --always --dirty=-dev)
COMMIT := $(shell git show-ref --hash refs/heads/master)
DATE := $(shell date --iso-8601=seconds --utc)
GOFILES = $(shell find . -type f -name '*.go')
UIFILES = package.json package-lock.json semantic.json $(shell find ui/semantic -type f )

all: cacert-boardvoting

cacert-boardvoting: ${GOFILES}
	go build -o $@ -buildmode=pie -trimpath -x -ldflags " -s -w -X 'main.version=${VERSION}' -X 'main.commit=${COMMIT}' -X 'main.date=${DATE}'"

clean:
	rm -f cacert-boardvoting

ui: ${UIFILES}
	npm install
	cd node_modules/fomantic-ui ; npx gulp build

.PHONY: clean distclean all ui
