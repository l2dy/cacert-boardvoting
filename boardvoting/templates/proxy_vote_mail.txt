Dear Board,

{{ .Proxy }} has just registered a proxy vote of {{ .Vote }} for {{ .Voter }} on motion {{ .Decision.Tag }}.

The justification for this was:
{{ .Justification }}

Motion:
{{ .Decision.Title }}
{{ .Decision.Content }}

Kind regards,
the vote system